---?include=slides/title.md

---?include=slides/what-is-git.md

---?include=slides/branching-and-merging.md

---?include=slides/git-commands.md

---?include=slides/git-workflow.md

---?include=slides/reference.md