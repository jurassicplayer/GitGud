## Branching and Merging

<div align="left">Allows for:</div>
@ul

- concurrent work on a project
- staging areas of product development
- maintenance of stable releases

@ulend

+++

## What is Branching?

@ul

- Cloning the original into a separate entity that can be modified
- Ex. Personal copies of a printout given to all employees

@ulend

+++

## What is Merging?

@ul

- Combining the changes from one branch into another
- Can remove the branch or continue using it
- Is where conflicts between branches get resolved

@ulend