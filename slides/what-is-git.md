## What is Git?

@ul

- An actively maintained open-source project
- A VCS originally developed in 2005 by Linus Torvalds

@ulend

+++

## What is a VCS?

@ul

- Stands for "Version Control System"
- Keeps track of who made what changes and when
- Branching and merging from concurrent work.
- Works as a backup that can be reverted to when SHTF

@ulend

+++

## What does all this mean?

@ul

- Git is the secretary you always wish you had
- Git keeps track of who made what changes, when, and where

@ulend