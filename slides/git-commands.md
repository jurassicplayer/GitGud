## Git Commands

@ul

- There are quite a few of them
- Most you don't come across regularly
- [Git Documentation](https://git-scm.com/docs)
- [Common Git Commands](http://guides.beanstalkapp.com/version-control/common-git-commands.html)

@ulend

Note:
Go through the common git commands and explain them.