## Git Workflow

@ul

- Can scale up in complexity
- Boils down to a simple model
- Make copy, Edit copy, Commit copy, Merge copy

@ulend

+++
## Workflow Examples

- [Git Branching and Merging Strategies](https://www.youtube.com/watch?v=to6tIdy5rNc)
- [EndPoint.com](https://www.endpoint.com/blog/2014/05/02/git-workflows-that-work)
- [Atlassian.com](https://www.atlassian.com/git/tutorials/comparing-workflows)

Note: 
Draw simple model and expand to explain the reference materials.
