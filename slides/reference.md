## Helpful References

- [Common Git Commands](http://guides.beanstalkapp.com/version-control/common-git-commands.html)
- [Git Documentation](https://git-scm.com/docs)
- [Git Cheatsheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf)
- [Interactive Git Cheatsheet](http://ndpsoftware.com/git-cheatsheet.html)

+++

## Workflow Examples

- [Git Branching and Merging Strategies](https://www.youtube.com/watch?v=to6tIdy5rNc)
- [EndPoint.com](https://www.endpoint.com/blog/2014/05/02/git-workflows-that-work)